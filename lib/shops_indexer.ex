defmodule Eversearch.ShopsIndexer do
  alias Eversearch.{IndexRecord,Repo,Shop,SearchResult,SearchQuery}
  require Logger

  @spec search(Map.t) :: SearchResult
  def search(params) do
    params |> SearchQuery.from_params() |> search_shops_index()
  end

  @spec start() :: :ok
  def start do
    Logger.info("ShopsIndexer performs cold warmup...")
    Repo.delete_all(IndexRecord)
    rows = Shop.all |> Enum.map(&Shop.indexise/1)
    Repo.insert_all(IndexRecord, rows)
    Logger.info("ShopsIndexer warmed up!")
    :ok
  end

  @spec search_shops_index(SearchQuery) :: SearchResult
  def search_shops_index(search_query) do
    search_query
    |> Shop.lookup_index_search_phrase
    |> (fn x ->
      Logger.info inspect(x)
      x
    end).()
    |> Shop.calculate_cost(search_query)
  end
end
