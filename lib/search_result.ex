defmodule Eversearch.SearchResult do
  # actions: dropdown | redirect
  defstruct weight: 0.0,
    redirect_url: "",
    key: 0,
    hits: [],
    action: :redirect,
    additional: %{},
    products_result: false,
    type: "sometype",
    translated_type: "sometype"
end
