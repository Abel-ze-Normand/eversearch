defmodule Eversearch.SearchQuery do
  defstruct phrase: "", city: "", city_id: "", action: :redirect
  alias Eversearch.SearchQuery
  # actions: :redirect | :dropdown

  @spec from_params(Map.t) :: SearchQuery
  def from_params(params) do
    %SearchQuery{
      phrase:        params["search_phrase"],
      city:          params["city"],
      city_id:       params["city_id"],
      action:        params["action"]
    }
  end
end
