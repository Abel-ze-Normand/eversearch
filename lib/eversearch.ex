defmodule Eversearch do
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec

    # Define workers and child supervisors to be supervised
    children = [
      # Start the Ecto repository
      supervisor(Eversearch.Repo, []),
      # Start the endpoint when the application starts
      supervisor(Eversearch.Endpoint, []),
      # Start your own worker by calling: Eversearch.Worker.start_link(arg1, arg2, arg3)
      # worker(Eversearch.Worker, [arg1, arg2, arg3]),
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Eversearch.Supervisor]
    supervisor_res = Supervisor.start_link(children, opts)

    # Additional initializations
    wait_repo()

    additional_initializers()
    |> Enum.map(&Task.async/1)
    |> Task.yield_many()
    supervisor_res
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    Eversearch.Endpoint.config_change(changed, removed)
    :ok
  end

  def additional_initializers() do
    [
      &Eversearch.CatalogApiClient.start/0,
      &Eversearch.ShopsIndexer.start/0,
      &Eversearch.Prefixer.start/0
    ]
  end

  def wait_repo() do
    do_wait(true)
  end

  def do_wait(true) do
    (Process.whereis(Eversearch.Repo) == nil) |> do_wait()
  end

  def do_wait(false) do
    []
  end
end
