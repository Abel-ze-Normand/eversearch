defmodule Eversearch.CatalogApiClient do
  alias Eversearch.SearchResult
  require Logger
  use HTTPoison.Base

  def start() do
    {:ok, _} = super()
    :ok
  end

  def search(params) do
    params |> to_api_format |> request_api() |> cons_search_result()
  end

  # TODO conversions based on "redirect" and "dropdown" actions

  def to_api_format(params) do
    Map.merge(params, %{"phrase" => params["search_phrase"]})
  end

  def process_url(url) do
    get_default_url() <> url
  end

  def process_request_headers(headers) do
    [auth_header(username(), password())|super(headers)]
  end

  defp request_api(params) do
    get!("/products/drop", [], params: params) |> unpack_response()
  end

  defp unpack_response(response) do
    response.body
  end

  defp cons_search_result(raw_response) do
    response = raw_response |> Poison.decode!()
    Logger.debug "total: #{inspect response}"
    %SearchResult{
      weight: response["total"] * 0.05,
      additional: response,
      products_result: true,
      type: "material",
      translated_type: "Товары"
    }
  end

  defp get_default_url do
    System.get_env("CATALOG_API") || Application.get_env(:eversearch, :catalog_api)
  end

  defp auth_header(nil, nil) do
    nil
  end

  defp auth_header(username, password) do
    encoded = Base.encode64("#{username}:#{password}")
    {"Authorization", "Basic #{encoded}"}
  end

  defp username do
    System.get_env("CATALOG_API_USERNAME") || Application.get_env(:eversearch, :catalog_api_username)
  end

  defp password do
    System.get_env("CATALOG_API_PASSWORD") || Application.get_env(:eversearch, :catalog_api_password)
  end
end
