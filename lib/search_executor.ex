defmodule Eversearch.SearchExecutor do
  alias Eversearch.{ShopsIndexer,CatalogApiClient,SearchResult,Prefixer}
  require Logger

  @spec search(Map.t) :: String.t
  def search(params) do
    params
    |> start_search()
    |> collect_results()
    |> find_best_decision()
    # |> to_json()
  end

  @doc """
  All listed there functions must invoke search just by bypassing to them
  params from search string, passed from frontend
  """
  @spec registered_search_sources() :: [(Map.t -> SearchResult)]
  def registered_search_sources do
    [
      &ShopsIndexer.search/1,
      &CatalogApiClient.search/1,
      &Prefixer.search/1,
    ]
  end

  @spec start_search(Map.t) :: [Task.t]
  def start_search(params) do
    for f <- registered_search_sources(), do: Task.async(fn -> f.(params) end)
  end

  @spec collect_results([Task.t]) :: [SearchResult]
  defp collect_results(tasks) do
    for {_, {:ok, res}} <- Task.yield_many(tasks, 2000) do
      Logger.info inspect(res)
      res
    end
  end

  @spec find_best_decision([SearchResult]) :: SearchResult
  defp find_best_decision(search_results) do
    product_item = search_results |> Enum.find(fn (r) -> r.type == "material" and r.weight > 0.0 end)
    rest_items = search_results |> List.delete(product_item) |> Enum.filter(&(&1.weight > 0.0))
    cons_search_result(product_item, rest_items)
  end

  defp cons_search_result(nil, []) do
    %SearchResult{}
  end

  defp cons_search_result(nil, search_results) do
    Enum.max_by(search_results, &(&1.weight))
  end

  defp cons_search_result(product_item, []) do
    product_item
  end

  defp cons_search_result(product_item, search_results) do
    %SearchResult{
      redirect_url: product_item.redirect_url,
      key: product_item.key,
      hits: search_results,
      action: product_item.action,
      additional: product_item.additional,
      products_result: true,
      type: "mixed"
    }
  end
end
