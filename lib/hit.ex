defmodule Eversearch.Hit do
  defstruct link: "", weight: 0.0, key: "", text: ""
end
