defmodule Eversearch.Prefixer do
  alias Eversearch.{SearchResult,SearchQuery,Repo,Prefix}
  require Logger

  @spec search(Map.t) :: SearchResult
  def search(params) do
    params |> SearchQuery.from_params() |> search_prefix_index()
  end

  @spec start :: :ok
  def start do
    Logger.info("Prefixer performing cold warmup...")
    Repo.delete_all(Prefix)
    rows = Prefix.prefixes() |> Enum.map(&Prefix.indexise/1)
    Repo.insert_all(Prefix, rows)
    Logger.info("Prefixer warmed up!")
  end

  @spec search_prefix_index(SearchQuery) :: SearchResult
  defp search_prefix_index(search_query) do
    Prefix.search(search_query)
  end
end
