defmodule Eversearch.Repo.Migrations.CreateIndexTable do
  use Ecto.Migration

  def up do
    create table(:eversearch_indices) do
      add(:entity_key, :integer)
      add(:entity_type, :string)
      add(:tokens, {:array, :string}, default: [])
      add(:weight, :float, default: 1.0)
      add(:city_id, :string, size: 20)
      add(:name, :string)
    end

    execute "CREATE INDEX eversearch_indices_tokens_index ON \"eversearch_indices\" USING GIN(\"tokens\")"

    create table(:eversearch_prefixes) do
      add(:prefix, :string)
      add(:entity_type, :string)
      add(:redirect_link, :string)
      add(:text, :string)
      add(:weight, :float, default: 1.0)
    end

    create index(:eversearch_prefixes, [:prefix])
  end

  def down do
    drop index(:eversearch_prefixes, [:prefix])
    drop table(:eversearch_prefixes)
    drop index(:eversearch_indices, [:tokens])
    drop table(:eversearch_indices)
  end
end
