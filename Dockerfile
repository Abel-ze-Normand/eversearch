FROM elixir:1.4.2
MAINTAINER Nail Gibaev
ENV CATALOG_API https://catalog-api-dev.sdvor.com
ENV EVERSEARCH_PORT 4891
ENV CATALOG_DB_USERNAME sdvorcatalog
ENV CATALOG_DB_PASSWORD mIa022N8jQ1l
ENV CATALOG_DB_DBNAME sdvorcatalog
ENV CATALOG_DB_HOSTNAME 78.155.223.90
ENV MIX_ENV prod
ENV VERSION 0.0.1
EXPOSE $EVERSEARCH_PORT
RUN mkdir /app
WORKDIR /app
COPY . /app/
RUN mix do local.hex --force, \
local.rebar --force, \
deps.get --only-prod, \
deps.compile, \
compile, \
ecto.migrate, \
release
ENTRYPOINT ["rel/eversearch/bin/eversearch"]
CMD ["foreground"]
