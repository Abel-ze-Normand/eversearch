# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :eversearch,
  ecto_repos: [Eversearch.Repo]

# Configures the endpoint
config :eversearch, Eversearch.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "YkRjLPSEJWPcv4dAsNU4pBlqHRvzrw969vAIB2fGX7Iv9u/AA5UQIf/l5mp60AcJ",
  render_errors: [view: Eversearch.ErrorView, accepts: ~w(json)],
  pubsub: [name: Eversearch.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
