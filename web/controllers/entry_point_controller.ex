defmodule Eversearch.EntryPointController do
  alias Eversearch.SearchExecutor
  use Eversearch.Web, :controller
  require Logger

  def search(conn, params) do
    resp = SearchExecutor.search(params)
    json conn, resp
  end
end
