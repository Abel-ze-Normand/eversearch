defmodule Eversearch.Router do
  use Eversearch.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", Eversearch do
    pipe_through :api
  end

  get "/search", Eversearch.EntryPointController, :search
end
