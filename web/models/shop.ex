defmodule Eversearch.Shop do
  alias Eversearch.{Shop,IndexRecord,SearchQuery,SearchResult,Hit,Repo}
  use Eversearch.Web, :model
  require Stemex
  require Logger
  require Gibran

  @tokenizer_re ~r/\.|\,|\-|\s/
  @redirect_path "contacts"
  @max_result_size 10
  @stop_words ["магазин"]

  schema "catalog_shops" do
    field :city_id, :string, size: 20
    field :name, :string
    field :type_address, :string
    field :type_company, :string
    field :address, :string
    field :lon, :float
    field :lat, :float
    field :working_hours_from, :integer
    field :working_minutes_from, :integer
    field :working_hours_to, :integer
    field :working_minutes_to, :integer
    field :camera_link, :integer
    field :show_phone_on_site, :boolean
    field :show_in_block_hours, :boolean
    field :show_in_block_schemas, :boolean
    field :sort, :integer
    field :affiliate_id, :string, size: 20
    field :created_at, :utc_datetime
    field :updated_at, :utc_datetime
  end

  @spec all() :: List.t
  def all do
    Repo.all(from s in Shop,
      where: s.type_address == "magazin" or s.type_address == "rc",
      select: s)
  end

  @spec indexise(Shop) :: IndexRecord
  def indexise(shop) do
    search_name = "#{translate_type_address(shop.type_address)} #{shop.address}"
    tokens = search_name
    |> Gibran.from_string(:uniq_tokens, opts: [pattern: @tokenizer_re])
    |> Enum.map(&Stemex.russian/1)
    %{
      entity_key: shop.id,
      entity_type: "shop",
      tokens: tokens,
      city_id: shop.city_id,
      name: search_name
    }
  end

  @spec lookup_index_search_phrase(SearchQuery) :: [any]
  def lookup_index_search_phrase(query) do
    search_tokens = query.phrase
    |> Gibran.from_string(:uniq_tokens, opts: [pattern: @tokenizer_re])
    |> Enum.map(&Stemex.russian/1)

    exec_query!(search_tokens -- @stop_words, query)
    |> process_result(query)
  end

  defp exec_query!([], _) do
    []
  end

  defp exec_query!([token] = _tokens, query) do
    Ecto.Adapters.SQL.query!(
      Repo,
      [
        "SELECT id, entity_key, entity_type, tokens, weight, city_id, name ",
        "FROM (select id, entity_key, entity_type, tokens, weight, city_id, name, unnest(tokens) token FROM eversearch_indices) t ",
        "WHERE token LIKE '%' || $1 || '%' AND city_id = $2"
      ],
      [token, query.city_id]
    )
    |> extract_rows()
  end

  defp exec_query!(tokens, query) when length(tokens) > 1 do
    Ecto.Adapters.SQL.query!(
      Repo,
      [
        "SELECT * FROM eversearch_indices ",
        "WHERE tokens @> $1 AND city_id = $2"
      ],
      [tokens, query.city_id]
    )
    |> extract_rows()
  end

  defp exec_query!(_, _) do
    []
  end

  defp extract_rows(res) do
    res.rows
  end

  defp process_result(res, query) do
    res
    |> Stream.map(fn x -> to_shop_hit(x, query.city) end)
    |> Enum.into([])
    |> Enum.sort_by(fn x -> x.weight end)
  end

  @spec calculate_cost([Hit], SearchQuery) :: SearchResult
  def calculate_cost([], _) do
    %SearchResult{type: "shop", translated_type: "Магазины"}
  end

  def calculate_cost(hits, %SearchQuery{action: "redirect"}) do
    [top|_] = hits
    %SearchResult{
      weight: top.weight,
      redirect_url: top.link,
      key: top.weight,
      action: :redirect,
      type: "shop",
      translated_type: "Магазины"
    }
  end

  def calculate_cost(hits, %SearchQuery{action: "dropdown"}) do
    sum_weight = Enum.reduce(hits, 0, fn (x, acc) -> acc + x.weight end)
    %SearchResult{
      weight: sum_weight,
      hits: Enum.take(hits, @max_result_size),
      action: :dropdown,
      type: "shop",
      translated_type: "Магазины"
    }
  end

  defp to_shop_hit([_, entity_key, "shop", _, weight, _, name], city_name) do
    #%Hit{link: "#{city_name}/#{@redirect_path}?shop=#{entity_key}", weight: weight, key: entity_key, text: name}
    %Hit{link: "#{city_name}/#{@redirect_path}?key=#{entity_key}", weight: weight, key: entity_key, text: name}
  end

  # defp to_shop_hit(%IndexRecord{entity_type: :shop, entity_key: key, name: name, weight: weight}, city_name) do
  #   %Hit{link: "#{city_name}/#{@redirect_path}/#{key}", weight: weight, key: key, text: name}
  # end

  defp translate_type_address("magazin"), do: "Магазин"
  defp translate_type_address("rc"), do: "РЦ"
  defp translate_type_address(_), do: "" # throw :unsupported_type_address
end
