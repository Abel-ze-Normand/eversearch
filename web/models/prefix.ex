defmodule Eversearch.Prefix do
  alias Eversearch.{Prefix,SearchQuery,SearchResult,Repo,Hit}
  require Gibran
  use Eversearch.Web, :model
  require Logger

  schema "eversearch_prefixes" do
    field :prefix, :string
    field :entity_type, :string
    field :redirect_link, :string
    field :text, :string
    field :weight, :float
  end

  @spec indexise({String.t, Atom.t, String.t, String.t, Float.t}) :: Prefix
  def indexise({prefix, kind, link, text, weight}) do
    %{
      prefix: prefix,
      entity_type: kind,
      redirect_link: link,
      text: text,
      weight: weight,
    }
  end

  def prefixes do
    [
      {"магазины", "shop", "contacts", "Магазины", 0.9},
      {"филиалы", "shop", "contacts", "Магазины", 0.8},
      {"товар", "material", "catalog", "Каталог", 0.7},
    ]
  end

  @spec search(SearchQuery) :: SearchResult
  def search(search_query) do
    prefix = search_query.phrase |> Gibran.from_string(:uniq_tokens) |> hd()
    Prefix
    |> where([p], like(p.prefix, ^("#{prefix}%")))
    |> order_by([p], p.weight)
    |> Repo.all()
    |> process_result(search_query)
  end

  @spec process_result([Prefix], SearchQuery) :: SearchResult
  defp process_result([], _) do
    %SearchResult{}
  end

  defp process_result(prefix_list, %SearchQuery{action: "redirect"} = q) do
    [top_result|_] = prefix_list
    %SearchResult{
      weight: top_result.weight,
      redirect_url: "#{q.city}/#{top_result.redirect_link}",
      action: :redirect,
      type: top_result.entity_type,
      translated_type: top_result.text
    }
  end

  defp process_result(prefix_list, %SearchQuery{action: "dropdown"} = q) do
    hits = prefix_list
    |> Enum.map(fn x ->
      %Hit{
        link: "#{q.city}/#{x.redirect_link}",
        weight: x.weight,
        text: x.text
      }
    end)
    total_weight = hits |> Enum.reduce(0, fn (x, acc) -> acc + x.weight end)
    %SearchResult{
      weight: total_weight,
      hits: hits,
      action: :dropdown,
      type: hd(prefix_list).entity_type,
      translated_type: hd(prefix_list).text
    }
  end
end
