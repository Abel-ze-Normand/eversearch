defmodule Eversearch.IndexRecord do
  use Eversearch.Web, :model

  schema "eversearch_indices" do
    field :entity_key, :integer
    field :entity_type, :string
    field :tokens, {:array, :string}
    field :weight, :float
    field :city_id, :string
    field :name, :string
  end
end
